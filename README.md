# dot-files

This holds my dot-files for my OSes

- The `ssh-add.desktop` file will need to be placed in `~/.config/autostart/`
- The `ssh_askpass.conf` file will need to be placed in `~/.config/environment.d/`
- The Plasma config files will need to be placed in `~/.config/`

Plasma files: (configs)

- `configs/kwinrc` : This has my setting for the Super key (Meta in Plasma) behavior
- `configs/kglobalshortcutsrc` : This has window control shortcuts like moving between Workspaces (Desktops in Plasma)
- `configs/krunnerrc` : Sets KRunner to in the center of the screen when activated
- `configs/kdeglobals` : Sets the theme to Breeze Dark

Plasma work files: (configs)

- `configs/kwinrc` : This has my setting for the Super key (Meta in Plasma) behavior
- `configs/kglobalshortcutsrc` : This has window control shortcuts like moving between Workspaces (Desktops in Plasma)
- `configs/krunnerrc` : Sets KRunner to in the center of the screen when activated
- `configs/kdeglobals` : Sets the theme to Breeze Dark
- `configs/plasma-org.kde.plasma.desktop-appletsrc` : Has Panel and Applet data
- `configs/plasmashellrc` : Has Panel thickness and other information

## How to restore them

```
curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/Plasma/kdeglobals > kdeglobals; mv kdeglobals /home/$USER/.config
curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/Plasma/kglobalshortcutsrc > kglobalshortcutsrc; mv kglobalshortcutsrc /home/$USER/.config
curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/Plasma/krunnerrc > krunnerrc; mv krunnerrc /home/$USER/.config
curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/Plasma/kwinrc > kwinrc; mv kwinrc /home/$USER/.config
curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/Plasma/plasmashellrc > plasmashellrc; mv plasmashellrc /home/$USER/.config
curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/Plasma/plasma-org.kde.plasma.desktop-appletsrc > plasma-org.kde.plasma.desktop-appletsrc; mv plasma-org.kde.plasma.desktop-appletsrc /home/$USER/.config
```

# Setup SSH

```
mkdir /home/$USER/.config/autostart/
mkdir /home/$USER/.config/environment.d/
curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/ssh-add.desktop > ssh-add.desktop; mv ssh-add.desktop /home/$USER/.config/autostart/
curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/ssh_askpass.conf > ssh_askpass.conf; mv ssh_askpass.conf /home/$USER/.config/environment.d/
```

# Adjust for HiDPI displays

 - System Settings -> Display and Monitor -> Global scale -> 200%
 - System Settings -> Appearance -> Cursors -> Size -> 36

# Setup ProtonMail Bridge

## Create a passphrase-free GPG key non-interactively

`gpg --batch --passphrase '' --quick-gen-key 'ProtonMail Bridge' default default never`

## Setup pass

`pass init "ProtonMail Bridge"`

## Open ProtonMail Bridge and login

Credits : https://pychao.com/2020/06/10/update-on-using-protonmail-bridge-on-headless-wordpress-linux-servers/
